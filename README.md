# library-belpay-frontend

La siguiente información sirve como referencia para empezar a trabajar con BelPaySDK. A continuación se mostrará en código una implementación que permitirá al desarrollador entender cómo puede empezar a trabajar con nuestro SDK.

## Instalar la librería:

Para poder hacer uso de la librería en un navegador web, debe ubicarse en el footer de su página y deberá hacer lo siguiente:

```javascript
<script type="module" src="./belpay.min.js"></script>
```

#### NOTA

```
La librería se encuentra en el directorio dist de este repositorio.
```

## Instanciar la librería:

Inmediatamente después de instalar la librería deberá hacer lo siguiente:

```javascript
<script type="text/javascript">

    class BelpayDemo {
      // This constants can be modified dynamically
      const username = '';
      const password = '';
      const key = '';
      const environment = 'test'; // 'test' or 'prod'

      constructor() {
        this.belpay = new belpay.SDK({
          username: '', // put here your username_oauth_belcorp
          password: '', // put here your password_oauth_belcorp
          key     : '', // put here your api_key_payments_belcorp
        },'test');
      }
    }

    const demo = new BelpayDemo();
</script>
```

La forma de instanciar anterior es solo a modo de ejemplo, el desarrollador puede usar el método que crea conveniente, solo debe considerar que al momento de crear la instancia, con excepción del **segundo** parámetro de instanciación, **_todos los campos del objeto son obligatorios_**.

El segundo parámetro de la instancia en el ejemplo tiene _'test'_, sin embargo, en caso de usar la librería en producción deberá cambiarlo por _'prod'_ o simplemente no poner nada dejando la instanciación del SDK así:

```javascript
<script type="text/javascript">

    // 1st method, with 'prod' parameter.
    class BelpayDemo {
      constructor() {
        this.belpay = new belpay.SDK({
          username: '', // put here your username_oauth_belcorp
          password: '', // put here your password_oauth_belcorp
          key     : '', // put here your api_key_payments_belcorp
        }, 'prod');
      }
    }
    // 2nd method, without 'prod' parameter.
    class BelpayDemo {
      constructor() {
        this.belpay = new belpay.SDK({
          username: '', // put here your username_oauth_belcorp
          password: '', // put here your password_oauth_belcorp
          key     : '', // put here your api_key_payments_belcorp
        });
      }
    }

    const demo = new BelpayDemo();
</script>
```

## Estructura HTML

BelpaySDK se alimenta básicamente con la información mencionada anteriormente en la instancia de la librería, sin embargo, necesita obtener dicha información de algún formulario HTML. Aquí tenemos un modelo que podría usar:

```html
<input type="text" id="cardNumber" />
<input type="text" id="cardExpirationMonth" />
<input type="text" id="cardExpirationYear" />
<input type="text" id="cvv" />
<select id="cardType">
  <option value="visa">Visa</option>
  <option value="master">Mastercard</option>
  <option value="amex">American Express</option>
  <option value="discover">Discover</option>
  <option value="diners">Diners Club</option>
  <!-- and many others options -->
</select>
<input type="button" value="Enviar" id="btnEnviar" />
```

### Obtener los datos del formulario y tokenizar

A continuación se ofrece una forma de acceder a los elementos del formulario con Javascript vanilla (Usted puede usar el método que se adapte a sus necesidades).

```javascript
window.addEventListener('load', () => {
  // En este caso decidimos crear la instancia anterior al momento de cargar la página.
  demo = new BelpayDemo();
});

const button = document.getElementById('btnEnviar');

button.addEventListener('click', () => {
  const cardNumber = document.getElementById('cardNumber').value;
  const cardExpirationMonth = document.getElementById('cardExpirationMonth').value;
  const cardExpirationYear = document.getElementById('cardExpirationYear').value;
  const cardType = document.getElementById('cardType').value;
  const cvv = document.getElementById('cvv').value;

  // Tokenize process
  demo.belpay
    .tokenize({
      cardNumber,
      cardExpirationMonth,
      cardExpirationYear,
      cardType,
      cvv
    })
    .then((data) => {
      document.getElementById('cardNumber').value = data.maskedPan;
      document.getElementById('cardExpirationMonth').value = 'XX';
      document.getElementById('cardExpirationYear').value = 'XXXX';
    })
    .catch((err) => console.error(err));
});
```

## Resultado obtenido

Cuando el usuario haga clic en el botón enviar, debería tomar la respuesta exitosa de la petición a la api de cybersoruce y ver algo como lo siguiente:

![Resultado](images/result-tokenize.png)

## Procesar pago

### Markup
Se propone el siguiente marcado de código:

```html
Monto: 
<input type="number" name="amount" id="amount" value="100" />
Moneda:
<select name="currency" id="currency">
  <option value="USD" selected>Dollar americano</option>
  <option value="SOL">Nuevo sol</option>
  <option value="PES">Peso mexicano</option>
</select>
Nombre:
<input type="text" name="firstname" id="firstname" value="Luis" />
Apellidos:
<input type="text" name="lastname" id="lastname" value="Muñoz" />
Email:
<input type="email" name="email" id="email" value="luenmuvel@gmail.com" />
Área:
<input type="text" name="phone_area" id="phone_area" value="51" />
Número:
<input type="number" name="phone_number" id="phone_number" value="958969230" />
Zip code: 
<input type="text" name="zip_code" id="zip_code" value="Lima 39" />
Dirección: 
<input type="text" name="street_name" id="street_name" value="Ca. Carlos Monge" />
Número: 
<input type="text" name="street_number" id="street_number" value="541" />
Ciudad: 
<input type="text" name="locality" id="locality" value="Los Olivos" />
País: 
<input type="text" name="country" id="country" value="EC" />

<input type="button" value="make payment" id="makePayment" />
```

### Procesar pago: Javascript

Se propone la siguiente estructura de código javascript:

```javascript
const makePayment = document.getElementById('makePayment');
makePayment.addEventListener('click', () => {
  const amount = document.getElementById('amount').value;
  const currency = document.getElementById('currency').value;
  const firstname = document.getElementById('firstname').value;
  const lastname = document.getElementById('lastname').value;
  const email = document.getElementById('email').value;
  const phone_area = document.getElementById('phone_area').value;
  const phone_number = document.getElementById('phone_number').value;
  const zip_code = document.getElementById('zip_code').value;
  const street_name = document.getElementById('street_name').value;
  const street_number = document.getElementById('street_number').value;
  const locality = document.getElementById('locality').value;
  const country = document.getElementById('country').value;

  const infoCard = {
    payment_type_id: 'card',
    transaction_amount: amount,
    transaction_currency: currency,
    payer: {
      code_reference: '12345',
      first_name: firstname,
      last_name: lastname,
      email: email,
      identification: {
        number: '1077270572',
        type: 'NIT',
      },
      phone: {
        area_code: phone_area,
        number: phone_number,
      },
      address: {
        zip_code: zip_code,
        street_name: street_name,
        street_number: street_number,
        locality: locality,
        country: country,
      },
    },
    description: 'Prueba Automatizada',
    additional_info: [],
  };

  demo.makePayment(infoCard).then((data) => {
    const message = document.getElementById('message');
    message.innerHTML = 'Pago procesado con exito';
    alert('Pago realizadó con éxito');
  });
```

### Listar tarjetas: Javascript

Se propone la siguiente estructura de código javascript:

```javascript
const listCard = document.getElementById('listCard');
const codeReference = document.getElementById('payerCodeReference');
listCard.addEventListener('click', () => {

  demo.listCards(codeReference).then((data) => {
    const message = document.getElementById('message');
    message.innerHTML = data;
  });
```

Considerar que el código correspondiente al objeto `infoCard` es obligatorio y debe enviarse al método `makePayment()` como parámetro. Se sugiere observar el directorio `dist/` que ahí se encuentra el código demo completo.
### Iniciar subcripción pago: Javascript

Se propone la siguiente estructura de código javascript:

```javascript
     const iniciarSuscripcion = document.getElementById('btnIniciar')
      iniciarSuscripcion.addEventListener('click', () => {
        const userAgent = navigator.userAgent
        const returnUrl = 'http://localhost:1234/payment?belpay_session_id='
        const payment_type_id_p2p = document.getElementById('payment_type_id_p2p')
                .value;
        const montop2p = document.getElementById('monto_p2p')
                .value;
        const descripcionp2p = document.getElementById('descripcion_p2p')
                .value;
        const monedap2p = document.getElementById('moneda_p2p')
                .value;
        const subscribe = {
          userAgent,
          returnUrl,
          description: descripcionp2p,
          transaction_amount: montop2p,
          transaction_currency: monedap2p,
          payment_type_id: payment_type_id_p2p,
          type: 'payment_subscribe'
        }
        demo
                .subscribe(subscribe)
                .then((data) => {
                  window.location = data.response.processUrl
                })
                .catch((error) => console.log(error))
      })
```
Considerar el atributo `type:'payment_subscribe'` ya que este enviara hacia el pago suscripcion.
Considerar el atributo `returnUrl` ya que este realizara la rediccion hacia la pagina donde se realizar el pago.

### Realizar Pago: Javascript

Se propone la siguiente estructura de código javascript:

```javascript
     const pagarPtop = document.getElementById('btnPagar');
      pagarPtop.addEventListener('click', () => {
        const payment_type_id_p2p = document.getElementById('payment_type_id_p2p')
                .value;
        const montop2p = document.getElementById('monto_p2p')
                .value;
        const descripcionp2p = document.getElementById('descripcion_p2p')
                .value;
        const monedap2p = document.getElementById('moneda_p2p')
                .value;
        const codeReferencep2p = document.getElementById('code_reference_p2p')
                .value;
        const urlPath = window.location.href
        let params
        if (urlPath.indexOf('http://localhost:1234/payment?belpay_session_id=') != -1) {
          const queryString = window.location.search;
          const urlParams = new URLSearchParams(queryString);
          params = urlParams.get('belpay_session_id')
          console.log(params);
        }
        const paymentp2p = {
          description: descripcionp2p,
          transaction_amount: montop2p,
          transaction_currency: monedap2p,
          payment_type_id: payment_type_id_p2p,
          code_reference: codeReferencep2p,
          belpay_session_id: params,
          type : 'payment_subscribe'
        }
        demo
                .makePayment(paymentp2p)
                .then((data) => {
                  alert('Pago realizadó con éxito')
                })
                .catch((error) => console.log(error))
      })
```
Considerar el atributo `type:'payment_subscribe'` ya que este enviara hacia el pago suscripcion.
Considerar el atributo `payment?belpay_session_id=`, ya que por medio de este id obtendra el token que se encuentra 
en PlaceToPay para hacerlo parte del pago.

### Procesar Pago Basico: Javascript

Se propone la siguiente estructura de código javascript:

```javascript
      const pagarBasico = document.getElementById('btnPagar')
      pagarBasico.addEventListener('click', () => {
        const userAgent = navigator.userAgent
        const returnUrl = 'http://localhost:1234/pago_basico.html?belpay_session_id='
        const montop2p = document.getElementById('monto_p2p')
                .value;
        const descripcionp2p = document.getElementById('descripcion_p2p')
                .value;
        const monedap2p = document.getElementById('moneda_p2p')
                .value;
        const codereferencep2p = document.getElementById('code_reference_p2p')
                .value;
        const subscribe = {
          code_reference : codereferencep2p,
          userAgent,
          returnUrl,
          description: descripcionp2p,
          transaction_amount: montop2p,
          transaction_currency: monedap2p,
          type: 'payment_basic'
        }
        demo
                .subscribe(subscribe)
                .then((data) => {
                  window.location = data.response.processUrl
                })
                .catch((error) => console.log(error))
      })
```
Considerar el atributo `type:'payment_basic'` ya que este enviara hacia el pago basico.
Considerar el atributo `processUrl` ya que este realizara la rediccion hacia la url donde espera la confirmacion del pago.

### Confirmar Pago Basico: Javascript

Se propone la siguiente estructura de código javascript:

```javascript
                  const urlPath = window.location.href
                  if (urlPath.indexOf('http://localhost:1234/pago_basico.html?belpay_session_id=') != -1) {
                    const queryString = window.location.search;
                    const urlParams = new URLSearchParams(queryString);
                    params = urlParams.get('belpay_session_id')
                    console.log(params);
                    demo
                            .confirmPayment(params)
                            .then((data) => {
                              alert('pago con exitoso')
                            })
                            .catch((error) => console.log(error))
                  }
```
Considerar una validación sobre las url, cuando se hace la redireccion desde placetopay hacia el front.

### Tipos de documento
| Descripción                                                         | Código |
|---------------------------------------------------------------------|:------:|
| Carné permiso temporal                                              | `CPT`  |
| Carné residencial provisional                                       | `CRPR` |
| Carné residente permanente                                          | `CRPE` |
| Carnet de identidad                                                 | `CIDE` |
| Cédula de ciudadanía                                                | `CCIU` |
| Cédula de extranjería                                               | `CEX`  |
| Cédula de identidad                                                 | `CI`   |
| Documento de identificacion de caracter migratorio para extranjeros | `DICME`|
| Documento de identificacion ante la entidad tributaria              | `DIET` |
| Documento nacional de identidad                                     | `DNI`  |
| Documento único de identidad                                        | `DUI`  |
| Número de identificacion tributaria                                 | `NIT`  |
| Pasaporte                                                           | `PAS`  | 
| Registro Único de contribuyentes                                    | `RUC`  |
| RFC Federal de Contribuyentes                                       | `RFC`  |
| RFC Fiscal                                                          | `RFCF` |
| RFC Persona Natural                                                 | `RFCPN`|
| RFC Persona Moral	                                              | `RFCPM`|
| Otro	                                                              | `Otro` |

### Tipos de documento por país
| País        | Descripción                                                         | Código |
|-------------|---------------------------------------------------------------------|:------:|
| Bolivia     | Cédula de identidad                                                 | `CI`   |
| Bolivia     | Número de identificacion tributaria                                 | `NIT`  |
| Chile       | Rol único tributario                                                | `RUT`  |
| Chile	      | Otro	                                                            | `Otro` |
| Colombia    | Cédula de ciudadanía                                                | `CCIU` |
| Colombia    | Cédula de extranjería                                               | `CEX`  |
| Colombia    | Número de identificacion tributaria                                 | `NIT`  |
| Colombia    | Otro	                                                            | `Otro` |
| Costa Rica  | Cédula de ciudadanía                                                | `CI`   |
| Costa Rica  | Documento de identificacion ante la entidad tributaria              | `DIET` |
| Costa Rica  | Documento de identificacion de caracter migratorio para extranjeros | `DICME`|
| Costa Rica  | Pasaporte                                                           | `PAS`  |
| Dominicana  | Carnet de identidad                                                 | `CIDE` |
| Dominicana  | Cédula de ciudadanía                                                | `CI`   |
| Dominicana  | Número de identificacion tributaria                                 | `NIT`  |
| Dominicana  | RFC Fiscal                                                          | `RFCF` |
| Ecuador     | Número de identificacion tributaria                                 | `NIT`  |
| Ecuador     | Carnet de identidad                                                 | `CIDE` |
| Ecuador     | Cédula de identidad                                                 | `CI`   |
| Ecuador     | Número de identificacion tributaria                                 | `NIT`  |
| Ecuador     | Pasaporte                                                           | `PAS`  |
| El Salvador | Carnet de identidad                                                 | `CIDE` |
| El Salvador | Documento único de identidad                                        | `DUI`  |
| Guatemala   | Carnet de identidad                                                 | `CIDE` |
| Guatemala   | Cédula de identidad                                                 | `CI`   |
| Guatemala   | Número de identificacion tributaria                                 | `NIT`  |
| México      | RFC Federal de Contribuyentes                                       | `RFC`  |
| México      | RFC Persona Natural                                                 | `RFCPN`|
| México      | RFC Persona Moral	                                            | `RFCPM`|
| Panamá      | Carnet de identidad                                                 | `CIDE` |
| Panamá      | Carné permiso temporal                                              | `CPT`  |
| Panamá      | Carné residente provisional                                         | `CRPR` |
| Panamá      | Carné residente permanente                                          | `CRPE` |
| Panamá      | Documento nacional de identidad                                     | `DNI`  |
| Panamá      | Pasaporte                                                           | `PAS`  |
| Panamá      | Número de identificacion tributaria                                 | `NIT`  |
| Puerto Rico | Carnet de identidad                                                 | `CI`   |
| Puerto Rico | Cédula de ciudadanía                                                | `CCIU` |
| Puerto Rico | Número de identificacion tributaria                                 | `NIT`  |
| Puerto Rico | RFC Fiscal                                                          | `RFCF` |

### Tipos de pago
| Descripción        | Código        |
|--------------------|:-------------:|
| Tarjeta de débito  | `debit_card`  |
| Tarjeta de crédito | `credit_card` |

### Tipos de moneda
| País        | Moneda              | Código |
|-------------|---------------------|:------:|
| Bolivia     | Bolivianos          | `BOB` |
| Bolivia     | Dólares             | `USD` |
| Costa Rica  | Colón costarricense | `CRC` |
| Dominicana  | Peso dominicano     | `DOP` |
| Ecuador     | Dólares             | `USD` |
| El Salvador | Dólares             | `USD` |
| Guatemala   | Quetzales           | `GTQ` |
| Guatemala   | Dólares             | `USD` |
| México      | Pesos mexicanos     | `MXN` |
| Panamá      | Dólares             | `USD` |
| Puerto Rico | Dólares             | `USD` |

### Países
| País        | Código |
|-------------|:------:|
| Bolivia     | `BO`   |
| Colombia    | `CO`   |
| Costa Rica  | `CR`   |
| Dominicana  | `DO`   |
| Ecuador     | `EC`   |
| El Salvador | `SV`   |
| Guatemala   | `GT`   |
| México      | `MX`   |
| Panamá      | `PA`   |
| Puerto Rico | `PR`   |

### Procesar pago con token ya existente.

En caso ya cuente con un token y quiera procesar un pago sin necesidad de ejecutar la tokenización deberá hacer lo siguiente: el objeto `infoCard` del código anterior debe tener una clave de nombre `card` con las claves `token` y `security_code`.

```javascript
{
  payer: {
    code_reference: '12345',
  },
  // añadir esta clave y sus valores.
  card: {
    token: 'YOUR TOKEN HERE',
    security_code: 'YOUR CVV CODE HERE',
  },
}
```

Debe considerar que esta información es necesaria y requerida para poder hacer el pago. Es importante también considerar que la clave `code_reference` dentro del objeto `infoCard`.

## Métodos públicos del SDK:

### tokenize
Este método le permite tokenizar una tarjeta. El valor devuelto es un objeto con el token de la tarjeta tokenizada, el status y el maskedPan.

### getFingerprintId
Este método le permitirá obtener el identificador del fingerprint y se obtiene luego de hacer la tokenización.

### getStatusTransitions
Este método le permite conocer el estado del request transitions. Su valor es un booleano.

### makePayment
Este método recibe como parámetro un objeto de pagos (en el demo `infoCard`) y recibe una promesa.

### listCards
Este método recibe como parámetro un objeto de tarjetas (en el demo `getListCard`) y recibe una promesa.

### confirmPayment
Este método recibe como parámetro un objeto de pagos (en el demo `reference_code`) y recibe una promesa.

### subscribe
Este método recibe como parámetro un objeto de pagos (en el demo `subscribe`) y recibe una promesa.


## Changelog

### Versiones

* 1.0.0: Conexión y tokenización con proveedor Cybersource
* 1.1.0: Pagos con proveedor Cybersource
